import express from 'express'
import db from './db'
import bodyParser from 'body-parser'
import cors from 'cors'
import { middleware } from './authentication'
import _ from 'lodash'
import "core-js/stable";
import "regenerator-runtime/runtime";

import { login, createAccount, getProfile } from './api/accounts'
import { getOrdersByUser, createOrder } from './api/orders'
import { getProducts, getProductsById } from './api/products'

const corsList = process.env.CORS_LIST.split(';')
const corsOptions = {
    origin: (origin, callback) => {
        if (corsList.indexOf(origin) !== -1 || corsList.indexOf('*') !== -1) {
            callback(null, true)
        } else {
            console.log('not cors')
            callback(new Error('Not allowed by CORS'))
        }
    },
    methods: "GET,POST", //GET,HEAD,PUT,PATCH,POST,DELETE
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const app = express()
const port = process.env.PORT || 5000

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(require('request-param')())
app.use(cors(corsOptions))

app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
})

// Authentication
app.post('/login', login)

app.post('/accounts', createAccount)

// User Management
app.get('/accounts', middleware, getProfile)

app.get('/orders', middleware, getOrdersByUser)

// Product Management
app.get('/products', middleware, getProducts)

app.get('/products/:id', middleware, getProductsById)

// Order Management
app.post('/orders', middleware, createOrder)

app.delete('/orders', middleware, (req, res) => {
    //cancel order
    res.status(200).json({ success: true })
})

app.get('/orders', middleware, (req, res) => {
    //get orders detail
    res.status(200).json({ success: true })
})