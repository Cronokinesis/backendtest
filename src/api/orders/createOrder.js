import _ from 'lodash'
import excuteQuery from '../../db'
import moment from 'moment'

export async function createOrder(req, res) {
    try {
        // not compelete
        const { user_id, product_id, unit, total, price } = _.get(req, 'body', {})
        const now = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        const { error } = await excuteQuery({
            query: "INSERT INTO orders (user_id, product_id, unit, status, total, price, created_at, updated_at) VALUES(?,?,?,?,?,?,?,?)",
            values: [user_id, product_id, unit, 1, total, price, now, now]
        })
        if (_.isEmpty(error)) {
            res.status(200).json({
                success: true,
            })
        } else {
            res.status(200).json({
                success: false,
            })
        }

    } catch (e) {
        res.status(200).json({
            success: false,
        })
    }
}