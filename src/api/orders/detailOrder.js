import _ from 'lodash'
import excuteQuery from '../../db'
import moment from 'moment'

export async function detailOrder(req, res) {
    try {
        const { id } = _.get(req, 'headers.profile', -1)
        const response = await excuteQuery({
            query: "SELECT * FROM orders WHERE user_id = ?",
            values: [id]
        })

        res.status(200).json({
            success: true,
            orders: response
        })
    } catch (e) {
        res.status(200).json({
            success: false,
        })
    }
}