import _ from 'lodash'
import excuteQuery from '../../db'

export async function getProducts(req, res) {
    try {
        const response = await excuteQuery({
            query: "SELECT * FROM products",
            values: []
        })

        res.status(200).json({
            success: true,
            products: response
        })
    } catch (e) {
        res.status(200).json({
            success: false,
            token: ""
        })
    }
}


export async function getProductsById(req, res) {
    try {
        const id = _.get(req, 'params.id', -1)
        const response = await excuteQuery({
            query: "SELECT * FROM products WHERE id = ?",
            values: [id]
        })
        if (response.length == 1) {
            res.status(200).json({
                success: true,
                product: response[0]
            })
            return
        } else {
            res.status(200).json({
                success: false,
                error: 'not found id'
            })
            return 
        }
    } catch (e) {
        res.status(200).json({
            success: false,
        })
    }
}