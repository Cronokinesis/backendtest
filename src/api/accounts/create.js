import _ from 'lodash'
import excuteQuery from '../../db'
import moment from 'moment'

export async function createAccount(req, res) {
    try {
        const {
            username,
            password,
            email,
            firstname,
            lastname
        } = _.get(req, 'body', {})
        const now = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        const { error } = await excuteQuery({
            query: "INSERT INTO users (username, password, email, firstname, lastname, created_at, updated_at) VALUES(?,?,?,?,?,?,?)",
            values: [username, password, email, firstname, lastname, now, now]
        })

        if (!_.isEmpty(error)) {
            res.status(200).json({
                success: false,
                error
            })
            return
        }

        res.status(200).json({
            success: true,
            token: "123456"
        })
    } catch (e) {
        res.status(200).json({
            success: false,
            token: ""
        })
    }
}