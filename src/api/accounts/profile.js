import _ from 'lodash'

export async function getProfile(req, res) {
    try {
        const profile = _.get(req, 'headers.profile', -1)

        res.status(200).json({
            success: true,
            profile
        })
    } catch (e) {
        res.status(200).json({
            success: false,
            token: ""
        })
    }
}