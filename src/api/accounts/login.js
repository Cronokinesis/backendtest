import _ from 'lodash'
import excuteQuery from '../../db'
import { createToken } from '../../authentication'

export async function login(req, res) {
    try {
        const { username, password } = _.get(req, 'body', {})

        const response = await excuteQuery({
            query: "SELECT * FROM users",
            values: [username, password]
        })

        if (response.length == 1) {
            const user = response[0]
            res.status(200).json({
                success: true,
                token: createToken({
                    id: _.get(user, 'id'),
                    email: _.get(user, 'email'),
                    firstname: _.get(user, 'firstname'),
                    lastname: _.get(user, 'lastname'),
                    createdAt: _.get(user, 'created_at'),
                    updatedAt: _.get(user, 'updated_at')
                })
            })
            return
        }
        res.status(401).send('unauthentication')
        return
    } catch (e) {
        console.log(e)
        res.status(401).send('unauthentication')
        return
    }
}