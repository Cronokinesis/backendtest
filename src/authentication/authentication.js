import _ from 'lodash'
import jwt from 'jwt-simple'
import { verifyToken } from './token'

// const jwt = require("jwt-simple");
// //เพิ่มโค้ดลงไปใน app.post("/login")
// app.post("/login, loginMiddleware, (req, res) => {
//    const payload = {
//       sub: req.body.username,
//       iat: new Date().getTime()//มาจากคำว่า issued at time (สร้างเมื่อ)
//    };
//    const SECRET = "MY_SECRET_KEY"; //ในการใช้งานจริง คีย์นี้ให้เก็บเป็นความลับ
//    res.send(jwt.encode(payload, SECRET));
// }

export async function middleware(req, res, next) {
    try {
        const bearer = _.get(req, 'headers.authorization', '')
        if (!_.isEmpty(bearer) && bearer.indexOf('Bearer ') > -1) {
            const token = bearer.split(' ')[1]
            const { success, profile } = verifyToken(token)
            if (success) {
                req.headers.profile = profile
                next();
            } else {
                res.status(401).send("unauthenticated")
                return
            }
        } else {
            res.status(401).send("unauthenticated")
            return
        }
    } catch (e) {
        console.log(e)
        res.status(403).send('')
        return
    }
};
