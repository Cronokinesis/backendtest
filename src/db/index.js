import mysql from 'serverless-mysql';

const db = mysql({
    config: {
        host: "localhost",//process.env.MYSQL_HOST,
        port: 3306,//process.env.MYSQL_PORT,
        database: "project",//process.env.MYSQL_DATABASE,
        user: "root",//process.env.MYSQL_USER,
        password: "password"//process.env.MYSQL_PASSWORD
    }
});

export default async function excuteQuery({ query, values }) {
    try {
        const results = await db.query(query, values);
        await db.end();
        return results;
    } catch (error) {
        return { error };
    }
}